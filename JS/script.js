document.addEventListener('DOMContentLoaded', function() {
    let element1 = document.querySelector('ul.tabs-content>li:nth-child(1)');
    element1.id = 'Akali';
    document.getElementById('Akali').style.display = 'none';

    let element2 = document.querySelector('ul.tabs-content>li:nth-child(2)');
    element2.id = 'Anivia';
    document.getElementById('Anivia').style.display = 'none';

    let element3 = document.querySelector('ul.tabs-content>li:nth-child(3)');
    element3.id = 'Draven';
    document.getElementById('Draven').style.display = 'none';

    let element4 = document.querySelector('ul.tabs-content>li:nth-child(4)');
    element4.id = 'Garen';
    document.getElementById('Garen').style.display = 'none';

    let element5 = document.querySelector('ul.tabs-content>li:nth-child(5)');
    element5.id = 'Katarina';
    document.getElementById('Katarina').style.display = 'none';

    let tabsContent = Array.from(document.querySelectorAll('ul.tabs-content > li'));
    let tabsTitle = document.getElementsByClassName('tabs-title');

    const container = document.addEventListener('click', (event) => {
        const classActive = document.querySelector('.active');
        classActive.classList.remove('active');
        event.target.classList.add('active');

        let clickedText = event.target.innerText;
        
        tabsContent.forEach(key =>{
            key.style.display = 'none';
        })

        tabsContent.forEach(key => {
            if (clickedText === key.id) {
                key.style.display = 'block';
            }
        });
    });
});